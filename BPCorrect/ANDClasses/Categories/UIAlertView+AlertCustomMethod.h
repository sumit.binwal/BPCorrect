//
//  UIAlertView+AlertCustomMethod.h
//  ANDMedical
//
//  Created by deepak.Gupta on 3/14/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (AlertCustomMethod)

+(void) showAlertWithDelayAndTitle:(NSString *)title message:(NSString *)message;
+(void) showAlertWithTitle:(NSString *)title message:(NSString *)message;
+(void) showAlertWithError:(NSError *)error shouldDisplayErrorTitle:(BOOL)shouldDisplayTitle;
+(void) showAlertWithException:(NSException *)exception shouldDisplayExceptionTitle:(BOOL)shouldDisplayTitle;

@end
