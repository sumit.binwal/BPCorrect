//
//  ANDWeightScaleDashboardCell.h
//  ANDMedical
//
//  Created by Ajay Chaudhary on 4/17/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ANDWeightScaleDashboardCell : UITableViewCell
{
    
}
@property (nonatomic, weak) IBOutlet UILabel   *lbl_weight;
@property (nonatomic, weak) IBOutlet UILabel   *lbl_weight_unit;

@property (weak, nonatomic) IBOutlet UILabel *lbl_BMI;
@property (weak, nonatomic) IBOutlet UILabel *lbl_BMI_unit;

@property (nonatomic, weak) IBOutlet UIButton  *btn_next;
@property (nonatomic, weak) IBOutlet UIButton  *btn_previous;
@property (nonatomic, weak) IBOutlet UIView    *view_base;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;

+(ANDWeightScaleDashboardCell *) getCell;
-(NSString *) reuseIdentifier;

@end
