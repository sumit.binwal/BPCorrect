//
//  ANDDropDown.h
//  ANDMedical
//
//  Created by Ajay Chaudhary on 4/24/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ANDDropDown;
@protocol ANDDropDownDelegate

- (void) DropDownDelegateMethod: (ANDDropDown *) sender;

@end
@interface ANDDropDown : UIView<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) id <ANDDropDownDelegate> delegate;

-(void)hideDropDown:(UIButton *)button;
- (id)showDropDownWithSender:(UIButton *)button withHeight:(CGFloat *)height tableArray:(NSArray *)arr andImageArray:(NSArray *)imgArray andDirection:(NSString *)direction;

@end
