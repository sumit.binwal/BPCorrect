//
//  CommonClass.h
//  ANDMedical
//
//  Created by deepak.Gupta on 2/10/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ANDMedCoreDataManager.h"

@interface CommonClass : NSObject

+ (id)fetchSSIDInfo;
+ (NSString *)getIPAddress;


+ (BOOL)is24HourFormat;
+(NSString *)getTimeStampFromDate:(NSDate *)date;
+(NSDate *)getDateFromTimeStamp:(NSString *)timeStamp;

+ (NSString *)appNameAndVersionNumberDisplayString;
+ (id)addingDashBoardButton : (id) viewController; //Sim modified for Cesar - To get dashboard icon on right
+ (id)addingHistoryBackButton : (id) viewController; //Sim added for Cesar
+ (id)addingPrintPDFButton : (id) target action:(SEL)action;
+ (id)addingLogoWithTitleOnNavBar : (id) viewController title : (NSString *)title_str BackButtonVisibility : (BOOL) backButton;
+ (id)removeTopBarView : (id)viewController;
+(void)slideView:(id)myView withX:(int)x withY:(int)y andAnimation:(float)animation;
+(void)increaseFrameOfView:(id)myView withWidth:(int)w withHeight:(int)h andAnimation:(float)animation;
+(void)popOutView:(id)myView withAnimation:(float)animationDuration withDelay:(float)delayFactor;
+(void)popInView:(id)myView withAnimation:(float)animationDuration withDelay:(float)delayFactor;

+(NSError *) getErrorForException:(NSException *)exception;
+(NSString *) getDeviceTypeForPeripheral:(NSString *)name;
+(NSString *) getHourMinValueFromTimeString:(NSString *)timeString;

+(CGSize) getHeightForWidht:(CGFloat)width andFont:(UIFont *)font forText:(NSString *)text;
+(CGSize) getWidthForHeight:(CGFloat)height andFont:(UIFont *)font forText:(NSString *)text;

/*
 Show alert methods...
 */
+(void) showAlertForError:(NSError *)error;
+(void) showAlertForException:(NSException *)exception;
+(void) showAlertForTitle:(NSString *)title
                  message:(NSString *)message;

+(BOOL) validateEmailAddress:(NSString *)emailID;
+(BOOL) validatePasswordText:(NSString *)pwdString;
+(NSString *) validatePassword:(NSString *)pwdString;

+ (BOOL)deviceModelName;

+(NSString *)dataFilePath;
+(void)deviceMeasurementType:(NSString *)type; //Sim added function to obtain the measurementType for the csv file
+ (BOOL)isLanguageJapanese;

/**
 Check Device LanguageCode.
 ex) en_UA -> compare「en」
 
 @param languageCode languageCode
 @return YES, Same Code.No, Other.
 */
+ (BOOL)isDeviceLanguageCode:(NSString *)languageCode;
/**
 Check Device CountryCode.
 ex) en_UA -> compare「UA」
 
 @param countryCode countryCode
 @return YES, Same Code.No, Other.
 */
+ (BOOL)isDeviceCountryCode:(NSString *)countryCode;
@end
