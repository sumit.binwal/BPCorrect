//
//  CommonClass.m
//  ANDMedical
//
//  Created by deepak.Gupta on 2/10/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "CommonClass.h"
#import <sys/utsname.h>
#import <SystemConfiguration/CaptiveNetwork.h>

#import <ifaddrs.h>
#import <arpa/inet.h>
#import "ANDMedGlobalConstants.h"

@implementation CommonClass
NSString* measurementType; //Sim added to append measurment type to the csv file

+ (BOOL)deviceModelName {
    
    BOOL status = TRUE;
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *machineName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *commonNamesDictionary =
    @{
    /* @"i386":     @"iPhone Simulator",
      @"x86_64":   @"iPad Simulator",
      
      @"iPhone1,1":    @"iPhone",
      @"iPhone1,2":    @"iPhone 3G",
      @"iPhone2,1":    @"iPhone 3GS",
      @"iPhone3,1":    @"iPhone 4",
      @"iPhone3,2":    @"iPhone 4(Rev A)",
      @"iPhone3,3":    @"iPhone 4(CDMA)",
      
    */
      
      @"iPhone4,1":    @"iPhone 4S",
      @"iPhone5,1":    @"iPhone 5(GSM)",
      @"iPhone5,2":    @"iPhone 5(GSM+CDMA)",
      @"iPhone5,3":    @"iPhone 5c(GSM)",
      @"iPhone5,4":    @"iPhone 5c(GSM+CDMA)",
      @"iPhone6,1":    @"iPhone 5s(GSM)",
      @"iPhone6,2":    @"iPhone 5s(GSM+CDMA)",
      @"iPhone7,1":    @"iPhone 6plus(GSM+CDMA)", //Sim changes made to include iPhone6 family for the app-WELWORLD-22
      @"iPhone7,2":    @"iPhone 6(GSM+CDMA)",
      @"iPhone8,1":   @"iPhone 6s",
      @"iPhone8,2":   @"iPhone 6s plus",
      @"iPhone8,4":    @"iPhone SE",    //Kunigita add iPhone SE
      @"iPhone9,1":   @"iPhone 7 1", //Sim added for iPhone 7
      @"iPhone9,3":   @"iPhone 7 2", //Sim added for iPhone 7
      @"iPhone9,2":   @"iPhone 7 Plus 1", //Sim added for iPhone 7
      @"iPhone9,4":   @"iPhone 7 Plus 2", //Sim added for iPhone 7
      @"iPhone10,1":  @"iPhone 8 1", //Sim added for iPhone 8
      @"iPhone10,4":  @"iPhone 8 2", //Sim added for iPhone 8
      @"iPhone10,2":  @"iPhone 8 Plus 1", //Sim added for iPhone 8 Plus
      @"iPhone10,5":  @"iPhone 8 Plus 2", //Sim added for iPhone 8 Plus
      @"iPhone10,3":  @"iPhone X 1", //Sim added for iPhone X
      @"iPhone10,6":  @"iPhone X 2", //Sim added for iPhone X
      
      
      
      @"iPad1,1":  @"iPad",
      @"iPad2,1":  @"iPad 2(WiFi)",
      @"iPad2,2":  @"iPad 2(GSM)",
      @"iPad2,3":  @"iPad 2(CDMA)",
      @"iPad2,4":  @"iPad 2(WiFi Rev A)",
      @"iPad2,5":  @"iPad Mini(WiFi)",
      @"iPad2,6":  @"iPad Mini(GSM)",
      @"iPad2,7":  @"iPad Mini(GSM+CDMA)",
      @"iPad3,1":  @"iPad 3(WiFi)",
      @"iPad3,2":  @"iPad 3(GSM+CDMA)",
      @"iPad3,3":  @"iPad 3(GSM)",
      @"iPad3,4":  @"iPad 4(WiFi)",
      @"iPad3,5":  @"iPad 4(GSM)",
      @"iPad3,6":  @"iPad 4(GSM+CDMA)",
      @"iPad4,1":  @"iPad Air(one)",
      @"iPad4,2":  @"iPad Air",
      @"iPad4,3":  @"iPad Air(three)", //Sim changes made to include iPhone Family6
      @"iPad4,4":  @"iPad mini 2", // Wifi
      @"iPad4,5":  @"iPad mini 2", // Wifi + Cellular (model A1490)
      @"iPad4,6":  @"iPad mini 2", // Wifi + Cellular (model A1491)
      @"iPad4,7":  @"iPad mini 3", // Wifi (model A1599)
      @"iPad4,8":  @"iPad Mini 3",
      @"iPad4,9":  @"iPad Mini 3(A1601)",
      @"iPad5,1":  @"iPad mini 4(WiFi)",  //Kunigita add iPad air 2 family.
      @"iPad5,2":  @"iPad mini 4(GSM)",
      @"iPad5,3":  @"iPad Air 2(WiFi)",
      @"iPad5,4":  @"iPad Air 2(GSM)",
      @"iPad6,3":  @"iPad Pro",         //Kunigita add iPad Pro family
      @"iPad6,4":  @"iPad Pro",
      @"iPad6,7":  @"iPad Pro",
      @"iPad6,8":  @"iPad Pro",
      @"iPad6,11": @"iPad 5",
      @"iPad6,12": @"iPad 5",
      @"iPad7,1" : @"iPad Pro 12.9 inch",
      @"iPad7,2" : @"iPad Pro 12.9 inch 2",
      @"iPad7,3" : @"iPad Pro 10.5 inch",
      @"iPad7,4" : @"iPad Pro 10.5 inch 2",

      
      @"iPod1,1":  @"iPod 1st Gen",
      @"iPod2,1":  @"iPod 2nd Gen",
      @"iPod3,1":  @"iPod 3rd Gen",
      @"iPod4,1":  @"iPod 4th Gen",
      @"iPod5,1":  @"iPod 5th Gen",
      @"iPod7,1":  @"iPod 6th Gen",
      };
    
    NSString *deviceName = commonNamesDictionary[machineName];
    
    if (deviceName == nil) {
        deviceName = machineName;
        status = FALSE;
    }
    
    return status;
}


+ (id)fetchSSIDInfo
{
    NSArray *ifs = (__bridge id)CNCopySupportedInterfaces();
    NSLog(@"%s: Supported interfaces: %@", __func__, ifs);
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSLog(@"%s: %@ => %@", __func__, ifnam, info);
        if (info && [info count]) {
            break;
        }
       
    }
    return info ;
}




// Get IP Address
+ (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

+ (BOOL)is24HourFormat
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
    
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    return [dateFormatter.dateFormat isEqualToString:@"H:mm:ss"];
}

+(NSString *)getTimeStampFromDate:(NSDate *)date
{
    NSLog(@"getTimeStampFromDate: %f",([date timeIntervalSince1970] * 1000));
    NSLog(@"ceil %@",[NSString stringWithFormat:@"%lli",[@(ceil([date timeIntervalSince1970] * 1000)) longLongValue]]);
    
    return [NSString stringWithFormat:@"%lli",[@(ceil([date timeIntervalSince1970] * 1000)) longLongValue]];
}

+(NSDate *)getDateFromTimeStamp:(NSString *)timeStamp
{
    NSTimeInterval timeInSecond = [timeStamp longLongValue]/1000.0;
    return [NSDate dateWithTimeIntervalSince1970:timeInSecond];
}

+ (NSString *)appNameAndVersionNumberDisplayString
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    return [NSString stringWithFormat:@"Version %@ (Build %@)",majorVersion, minorVersion];
}


#pragma mark - Slide any view with animation
+(void)popOutView:(id)myView withAnimation:(float)animationDuration withDelay:(float)delayFactor
{
    UIView *view = (id)myView;
    view.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:animationDuration delay:delayFactor options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        view.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
}


+(void)popInView:(id)myView withAnimation:(float)animationDuration withDelay:(float)delayFactor
{
    UIView *view = (id)myView;
    view.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:animationDuration delay:delayFactor options:UIViewAnimationOptionCurveEaseOut animations:^{
        view.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
}


+(void)slideView:(id)myView withX:(int)x withY:(int)y andAnimation:(float)animation
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:animation];
    CGRect rect = [myView frame];
    rect.origin.x = x;
    rect.origin.y = y;
    [myView setFrame:rect];
    [UIView commitAnimations];
}


+(void)increaseFrameOfView:(id)myView withWidth:(int)w withHeight:(int)h andAnimation:(float)animation
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:animation];
    CGRect rect = [myView frame];
    rect.size.width = w;
    rect.size.height = h;
    [myView setFrame:rect];
    [UIView commitAnimations];
}

//Sim added for Cesars new design
#pragma mark - Adding Share Button On UINavigationBar
+ (id)addingHistoryBackButton : (id) viewController
{
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:viewController action:@selector(btn_historyback_clicked:)];
    
    [anotherButton setImage:[UIImage imageNamed:@"back_arrow"]];
    
    return anotherButton;
    //  anotherButton = nil;
}

#pragma mark - Adding PDF Report Button UINavigationBar
+ (id)addingPrintPDFButton : (id) target action:(SEL)action
{
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Print"] style:UIBarButtonItemStylePlain target:target action:action];
}


#pragma mark - Adding Logo On UINavigationBar
+ (id)addingLogoWithTitleOnNavBar : (id) viewController title : (NSString *)title_str BackButtonVisibility : (BOOL) backButton
{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, -20, 320, 64)];
    [view setTag:1000];
    view.backgroundColor = [UIColor colorWithRed:33.0/255.0 green:143/255.0 blue:183.0/255.0 alpha:1.0];
    
    UIImageView *img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logo.png" ]];
    [img setFrame:CGRectMake(250, 26, 50, 30)];
    [view addSubview:img];
    
    if (backButton) {
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(15, 32, 8, 14)];
        [btnBack addTarget:viewController action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [view addSubview:btnBack];
        
    }
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(80, 24, 150, 30)];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont boldSystemFontOfSize:16.0];
    lbl.text = title_str;
    [view addSubview:lbl];
    //  anotherButton = nil;
    
    return view;
}


+ (id)removeTopBarView : (UIViewController *)viewController
{
    UIView *view  = [viewController.navigationController.navigationBar viewWithTag:1000];
    return view;
}


#pragma mark - Helper Methods...
+(NSError *) getErrorForException:(NSException *)exception
{
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:exception.description forKey:NSLocalizedDescriptionKey];
    
    NSError *error = [[NSError alloc] initWithDomain:exception.name
                                                code:1000
                                            userInfo:userInfo];
    
    return error;
}


+(NSString *) getDeviceTypeForPeripheral:(NSString *)name
{
    if (name != nil)
    {
        if ([name rangeOfString:@"A&D_UC-352"].location != NSNotFound) {
            return @"ws";
        }
        else if ([name rangeOfString:@"651"].location != NSNotFound ||
                 [name rangeOfString:@"BLP"].location != NSNotFound)
        {
            return @"bp";
        }
        else if ([name rangeOfString:@"Life Trak"].location != NSNotFound) {
            return @"activity";
        }else if ([name rangeOfString:@"UW-302"].location != NSNotFound) {
            return @"activity";
        }//WEL-484
    }
    
    return @"";
}


#pragma mark - Getting height and width w.r.t string text...
+(CGSize) getHeightForWidht:(CGFloat)width andFont:(UIFont *)font forText:(NSString *)text
{
    CGSize boundingSize = CGSizeMake(width, CGFLOAT_MAX);
    CGSize requiredSize = [text sizeWithFont:font
                           constrainedToSize:boundingSize
                               lineBreakMode:NSLineBreakByWordWrapping];
    
    return requiredSize;
}


+(CGSize) getWidthForHeight:(CGFloat)height andFont:(UIFont *)font forText:(NSString *)text
{
    CGSize expectedLabelSize = CGSizeZero;
    expectedLabelSize = [text sizeWithFont:font constrainedToSize:CGSizeMake(CGFLOAT_MAX, height)
                                    lineBreakMode:NSLineBreakByWordWrapping];
    
    return expectedLabelSize;
}


#pragma mark - Alert View Methods...
+(void) showAlertForTitle:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


+(void) showAlertForError:(NSError *)error
{
    [self showAlertForTitle:@"Error" message:error.localizedDescription];
}


+(void) showAlertForException:(NSException *)exception
{
    [self showAlertForTitle:exception.name message:exception.description];
}


+(NSString *) getHourMinValueFromTimeString:(NSString *)timeString
{
    NSMutableArray *timeParameters = [[timeString componentsSeparatedByString:@":"] mutableCopy];
  
    NSString *timestring = @"0.0";
    
    if ([timeParameters count]>1)
        timestring = [NSString stringWithFormat:@"%@.%@",[timeParameters objectAtIndex:0],[timeParameters objectAtIndex:1]];
    
    return timestring;
    
}


+(BOOL) validateEmailAddress:(NSString *)emailID
{
    NSString *emailRegEx =  @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
                            @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
                            @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
                            @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
                            @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
                            @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
                            @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:emailID];
    
    return myStringMatchesRegEx;
}

/*
+(BOOL) validatePasswordText:(NSString *)pwdString
{
    
    if ([pwdString length]<kMinimumPasswordLength)
        return FALSE;

    
    BOOL hasCaps = NO;
    BOOL hasAlphanumeric = NO;
    
    for (int i=0; i < pwdString.length; i++) {
        char ch = [pwdString characterAtIndex:i];
        
        if (ch >= 'A' && ch <= 'Z') {
            hasCaps = YES;
            break;
        }
    }
    
    for (int i=0; i < pwdString.length; i++) {
        char ch = [pwdString characterAtIndex:i];
        
        if (ch >= '0' && ch <= '9') {
            hasAlphanumeric = YES;
            break;
        }
    }

     return hasCaps && hasAlphanumeric ;
    
}
 */

+(NSString *) validatePassword:(NSString *)pwdString
{
    
    if ([pwdString length]<kMinimumPasswordLength)
        return @"Password length is too short. It should be atleast 7 characters.";
    
    BOOL hasCaps = NO;
    BOOL hasAlphanumeric = NO;
    
    for (int i=0; i < pwdString.length; i++) {
        char ch = [pwdString characterAtIndex:i];
        
        if (ch >= 'A' && ch <= 'Z') {
            hasCaps = YES;
            break;
        }
    }
    
    for (int i=0; i < pwdString.length; i++) {
        char ch = [pwdString characterAtIndex:i];
        
        if (ch >= '0' && ch <= '9') {
            hasAlphanumeric = YES;
            break;
        }
    }
    
    if (!hasCaps && !hasAlphanumeric)
        return @"Password must have atleast 1 capital letter and atleast 1 number.";
    else if (!hasCaps)
        return @"Password must have atleast 1 capital letter";
    else if (!hasAlphanumeric)
        return @"Password must have atleast 1 number";
    
    //return hasCaps && hasAlphanumeric ;
    
    return @"";
}


+(NSString *)dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //WELWORLD-65
    NSString *firstName = [[ANDMedCoreDataManager sharedManager].currentUser user_firstName];
    NSString *lastName = [[ANDMedCoreDataManager sharedManager].currentUser user_lastName];
    
    NSArray *tempArray = [[NSArray alloc] initWithObjects:firstName, lastName, measurementType, nil];
    NSString *tmpfilename = [tempArray componentsJoinedByString:@"-"]; //Separate each by a dot.
    NSArray *myStrings = [[NSArray alloc] initWithObjects:tmpfilename, @"csv", nil];
    NSString *filename = [myStrings componentsJoinedByString:@"."]; //Separate each by a dot.
    
    // NSString *filename = [measurementType stringByAppendingString:@"myfile.csv"]; //Code commented for WELWORLD-65
    return [documentsDirectory stringByAppendingPathComponent:filename];
}
+(void)deviceMeasurementType:(NSString *)type
{
    measurementType =  type;
}

//Sim added for Cesar
+ (id)addingDashBoardButton : (id) viewController
{
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:viewController action:@selector(btn_dashboard_clicked:)];
    
    [anotherButton setImage:[UIImage imageNamed:@"icon-menu-dashboard_large-@2x"]];
    
    return anotherButton;
    //  anotherButton = nil;
}

+ (BOOL)isLanguageJapanese {
    return [self isDeviceLanguageCode:@"ja"];
}

+ (BOOL)isDeviceLanguageCode:(NSString *)languageCode {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *code = [locale objectForKey:NSLocaleLanguageCode];
    return [languageCode isEqualToString:code];
}


+ (BOOL)isDeviceCountryCode:(NSString *)countryCode {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *code = [locale objectForKey:NSLocaleCountryCode];
    return [countryCode isEqualToString:code];
}

@end
