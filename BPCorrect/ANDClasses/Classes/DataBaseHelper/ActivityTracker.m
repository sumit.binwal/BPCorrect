//
//  ActivityTracker.m
//  ANDMedical
//
//  Created by Vishal Lohia on 3/6/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ActivityTracker.h"
#import "Users.h"
#import "ADActivityMonitor.h"
#import "ANDMedGlobalConstants.h"
#import "ANDMedCoreDataManager.h"
#import "ANDDateManager.h"
#import "WCTime.h"

@implementation ActivityTracker

@dynamic uuid;
@dynamic steps;
@dynamic sleep;
@dynamic calories;
@dynamic distances;
@dynamic created_date;
@dynamic modified_date;
@dynamic blockIndex;
@dynamic endTimeInterval;
@dynamic startTimeInterval;
@dynamic deleted_date;
@dynamic is_deleted;
@dynamic userActivity;
@dynamic sync_required;
@dynamic dateString;
@dynamic deviceType;
@dynamic bpm;
@dynamic measurementRecieveDateTime;
@dynamic unit;
@dynamic sleepStatus; //Sleep status parameter added for UW-302
@dynamic sleepHours; //Sleep hours sent by UW-302
@dynamic sleepMin; //Sleep min sent by UW-302
@dynamic calorie302; //Sim added to sync calories

-(NSString *) measurementDisplayDate
{
    return [WCTime printDateTimeWithDate:self.measurementRecieveDateTime forTimeType:displayType];
}


-(NSNumber *) DistanceAsPerUnit
{
    NSNumber *number = [NSNumber numberWithInt:0];
    
    if ([ANDMedCoreDataManager sharedManager].currentUser.is_metric.boolValue)//km
    {
        if ([self.unit isEqualToString:@"mile"])
        {
            number = self.distances;
        }
        else
        {
            number = self.distances;
        }
        number = self.distances;
    }
    else
    {
        if ([self.unit isEqualToString:@"mile"])
        {
            float mileDistance = self.distances.floatValue * 0.62137;
            NSString *dis = [NSString stringWithFormat:@"%0.2f",mileDistance];
            number = [NSNumber numberWithFloat:dis.floatValue];
        }
        else
        {
            float mileDistance = self.distances.floatValue * 0.62137;
            NSString *dis = [NSString stringWithFormat:@"%0.2f",mileDistance];
            number = [NSNumber numberWithFloat:dis.floatValue];
        }
        
        
    }
    
    return number;
}

+(NSArray *) getSortedStartDateActiveActivitiesForContext:(NSManagedObjectContext *)context
                                                  forUser:(Users *)usr
                                                    error:(NSError **)err
{
    @try {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
        NSSortDescriptor *sortDisc = [[NSSortDescriptor alloc] initWithKey:@"dateString"
                                                                 ascending:kIsActivityFetchOrderAscending];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userActivity.user_name = %@",usr.user_name];
        
        fetchRequest.entity = entity;
        fetchRequest.predicate = predicate;
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDisc];
        
        NSError *e = nil;
        NSArray *users = [context executeFetchRequest:fetchRequest
                                                error:&e];
        
        *err = e;
        
        return users;
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

+(void) fillTemporaryObjectForUser:(Users *)user inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"ActivityTracker" inManagedObjectContext:context];
    
    NSError *error = nil;
    NSArray *savedObjects = [context executeFetchRequest:request error:&error];
    
    NSDate *currentDate = [NSDate date];
    
    NSCalendarUnit unit = NSCalendarUnitDay | NSCalendarUnitMonth;
    NSDateComponents *dateComponent = [[NSCalendar currentCalendar] components:unit
                                                                      fromDate:currentDate];
    
    [dateComponent setDay:1];
    [dateComponent setMonth:1];
    
    NSDate *startDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
    
    [dateComponent setDay:31];
    [dateComponent setMonth:12];
    
    NSDate *endDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
    
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
    while ([startDate compare:endDate] != NSOrderedDescending)
    {
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd"];
        
        NSString *end = [df stringFromDate:endDate];
        [df setDateFormat:@"MM"];
        NSString *m = [df stringFromDate:startDate];
        
        NSMutableArray *a = [[NSMutableArray alloc] init];
        for (int i =1; i<=end.intValue; i++)
        {
            NSString *s = [df stringFromDate:startDate];
            [a addObject:s];
        }
        [d setObject:a forKey:m];
        
        
        
        
        NSDate *nextDate = [ANDDateManager getDayDateToNext:1 fromDate:startDate];
        startDate = nextDate;
    }
    
    NSLog(@"%@",d);
    
    /*
     NSDate *currentDate = [NSDate date];
     
     NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
     NSDateComponents *dateComponent = [[NSCalendar currentCalendar] components:unit
     fromDate:currentDate];
     
     [dateComponent setDay:1];
     [dateComponent setMonth:1];
     
     NSDate *startDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
     
     [dateComponent setDay:17];
     [dateComponent setMonth:5];
     
     NSDate *endDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
     
     while ([startDate compare:endDate] != NSOrderedDescending)
     {
     
     int randomIndex = arc4random() % savedObjects.count; // gives no .between 1 to 15 ..
     
     ActivityTracker *randomObject = savedObjects[randomIndex];
     NSString *dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:startDate];
     
     
     NSArray *steps = @[@1220, @1370, @1150, @990, @768, @1420, @1560];
     NSArray *cals = @[@300, @350, @280, @260, @240, @480, @620];
     NSArray *distance = @[@156, @189, @238, @256, @356, @678, @483];
     
     int randomValueIndex = arc4random() % steps.count; // gives no .between 1 to 15 ..
     
     ActivityTracker *tracker = [NSEntityDescription insertNewObjectForEntityForName:@"ActivityTracker"
     inManagedObjectContext:context];
     
     
     tracker.dateString = dateString;
     tracker.created_date = startDate;
     tracker.modified_date = startDate;
     
     tracker.deviceType = randomObject.deviceType;
     tracker.calories = cals[randomValueIndex];
     tracker.distances = distance[randomValueIndex];
     tracker.steps = steps[randomValueIndex];
     tracker.sleep = [NSNumber numberWithInt:0];
     tracker.uuid = randomObject.uuid;
     tracker.startTimeInterval = randomObject.startTimeInterval;
     tracker.endTimeInterval = randomObject.endTimeInterval;
     
     tracker.sync_required = [NSNumber numberWithBool:YES];
     tracker.measurementRecieveDateTime = startDate;
     
     [user addUserActivityObject:tracker];
     [tracker setUserActivity:user];
     
     NSError *errorSave = nil;
     if ([context save:&errorSave] == NO) {
     NSLog(@"Error Description :: %@",errorSave.description);
     }
     
     NSDate *nextDate = [ANDDateManager getDayDateToNext:1 fromDate:startDate];
     startDate = nextDate;
     }
     */
}

+(NSArray *) getSortedStartDateActiveActivitiesForContext:(NSManagedObjectContext *)context
                                                  forUser:(Users *)usr
                                                predicate:(NSPredicate *)p
                                                    error:(NSError **)err
{
    @try {
        
        //NSLog(@"%@",usr);
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
        NSSortDescriptor *sortDisc = [[NSSortDescriptor alloc] initWithKey:@"dateString"
                                                                 ascending:kIsActivityFetchOrderAscending];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userActivity.user_name = %@",usr.user_name];
        
        NSCompoundPredicate *compoundPredicate = nil;
        
        if (p)
            compoundPredicate = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType
                                                            subpredicates:@[p,predicate]];
        
        fetchRequest.entity = entity;
        fetchRequest.predicate = p ? (NSPredicate *)compoundPredicate : predicate;
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDisc];
        
        NSError *e = nil;
        NSArray *users = [context executeFetchRequest:fetchRequest
                                                error:&e];
        
        
        NSLog(@"%@",users);
        
        *err = e;
        
        return users;
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

+(void) insertORUpdateActivitiesRecords:(NSArray *)adActivityMonitorObjArray
                                forUser:(Users *)user
                             forContext:(NSManagedObjectContext *)context
                                  error:(NSError **)err;
{
     //Check wheter object already exists then we need to update it.
    //We have only dateTime parameter to check.
   
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];

    /*Below code is commented, as now we are doing smart sync, so we should not overwrite
     * the local database with all data received from server
     */
  /*  ADActivityMonitor *activityMonitor = [adActivityMonitorObjArray lastObject];
    if (activityMonitor.sync_type == SYNC_TYPE_SERVER) {
        NSLog(@"this is sync type server ");
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = entity;
        NSError *error = nil;
        NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userActivity.user_name = %@",[ANDMedCoreDataManager sharedManager].currentUser.user_name];
        request.predicate = userpredicate;
        NSArray *tempArray = [context executeFetchRequest:request error:&error];
        for (ActivityTracker *tr in tempArray) {
            [context deleteObject:tr];
        }
    } */
    
    NSLog(@"The count is %i", adActivityMonitorObjArray.count);
    for (ADActivityMonitor *activityMonitor in adActivityMonitorObjArray)
    {

        /*Sim-the below code is commented because the @"activity" is set based on the watch name and right
         now we should not limit the activity monitor connectivity to the watch name because it keeps changing
         */
        
      /*  if ([activityMonitor.deviceType isEqualToString:@"activity"] == NO) {
            continue;
        }*/
        

        activityMonitor.measurementReceivedDateTime = activityMonitor.measurementReceivedDateTime ? activityMonitor.measurementReceivedDateTime : [NSDate date];
        NSPredicate *predicate;
        
        if ([activityMonitor.dailyData isEqualToString:@"NA"]) {
            //If the data coming from cloud is of new design use the below predicate
            predicate = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@ AND userActivity.user_name = %@",activityMonitor.measurementReceivedDateTime,user.user_name];
            
            //Add logic then to get tracker and then overwrite it
        } else {
            //The data coming is of the old design , where we can overwrite date String
            predicate = [NSPredicate predicateWithFormat:@"dateString = %@ AND userActivity.user_name = %@",activityMonitor.dateString,user.user_name];
            //Write the old logic to overwrite it
        }
        
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = entity;
        request.predicate = predicate;
        NSError *error = nil;
        ActivityTracker *tracker = [[context executeFetchRequest:request error:&error] lastObject];
        NSDate *currentDate = [NSDate date];
        
        
        //Update if not nil
        if (tracker) {
            if ([activityMonitor.dailyData isEqualToString:@"NA"]) {
                //If its a new design, then just overwrite the obtained value with the stored value
                tracker.dateString = activityMonitor.dateString;
                tracker.calories = activityMonitor.calories;
                tracker.steps = activityMonitor.steps;
                tracker.distances = activityMonitor.distances;
                tracker.dailyData = activityMonitor.dailyData;
                tracker.dailyCalorieData = activityMonitor.dailyCalorieDate;
                tracker.dailyDistanceData = activityMonitor.dailyDistanceData;
                tracker.sleep = activityMonitor.sleep.intValue == 0 ? tracker.sleep : activityMonitor.sleep;
                tracker.calories = activityMonitor.calories;
                tracker.uuid = activityMonitor.uuid;
                tracker.startTimeInterval = activityMonitor.startTimeInterval;
                tracker.endTimeInterval = activityMonitor.endTimeInterval;
                tracker.modified_date = [NSDate date];
                tracker.sync_required = activityMonitor.sync_required;
                tracker.deviceType = activityMonitor.deviceType ;
                tracker.blockIndex = activityMonitor.blockIndex;
                tracker.unit = activityMonitor.unit;
                tracker.bpm = activityMonitor.bpm.intValue > 40 ? activityMonitor.bpm : tracker.bpm;
                //WEL-484, UW-302
                tracker.sleepStatus = activityMonitor.sleepStatus;
                tracker.sleepHours = activityMonitor.sleepHours;
                tracker.sleepMin = activityMonitor.sleepMin;
                tracker.calorie302 = activityMonitor.calorie302;
            
                NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
                [df2 setDateFormat:@"yy/MM/dd"];
                
                NSDate *savedMeasurementDate = [df2 dateFromString:activityMonitor.dateString];
                NSString *savedMeasurementDateString = [df2 stringFromDate:savedMeasurementDate];
                NSString *currentSystemDate = [df2 stringFromDate:[NSDate date]];
                tracker.measurementRecieveDateTime = activityMonitor.measurementReceivedDateTime ? activityMonitor.measurementReceivedDateTime : currentDate;
                
                
                NSError *errorUpdate = nil;
                if (activityMonitor.isRecordSavedFromThread) {
                    /*
                     if ([context hasChanges]) {
                     [context save:&errorUpdate];
                     [context reset];
                     }
                     */
                }
                else {
                    [context save:&errorUpdate];
                }

            } else {
                
                //Its old design so overwrite after a few checks -
                tracker.dateString = activityMonitor.dateString;
                
                //if ([tracker.uuid isEqualToString:activityMonitor.uuid]) {
                if ([tracker.calories doubleValue] < [activityMonitor.calories doubleValue]) {
                    tracker.calories = activityMonitor.calories;
                }
                if ([tracker.steps intValue] < [activityMonitor.steps intValue]) {
                    tracker.steps = activityMonitor.steps;
                }
                if ([tracker.distances doubleValue] < [activityMonitor.distances doubleValue]) {
                    tracker.distances = activityMonitor.distances;
                }
                if ([activityMonitor.bpm intValue] > 40) {
                    tracker.bpm = activityMonitor.bpm;
                    
                    
                }
                if(activityMonitor.dailyData.length > 3) {
                    //Check if all the steps is 0 then dont overwrite.
                    NSArray *arr = nil;
                    arr = [activityMonitor.dailyData componentsSeparatedByString:@";"];
                    BOOL is_valid = false;
                    
                    for (int i = 0; i < [arr count]; i++) {
                        NSString *address = [[arr objectAtIndex:i] stringByReplacingOccurrencesOfString:@"{" withString:@""];
                        address = [address stringByReplacingOccurrencesOfString:@"}" withString:@""];
                        address =  [address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        NSArray *temp = [address componentsSeparatedByString:@"="];
                        int value = [[temp lastObject] integerValue];
                        if (value != 0) {
                            is_valid = true;
                            break;
                        }
                    }
                    if (is_valid) {
                        //The activityMonitor object does have non zero value hence overwrite
                        tracker.dailyData = activityMonitor.dailyData; //Sim added for daily data design
                        is_valid = false;
                    } else {
                        //It does not have any non zero value hence do not overwrite
                    }
                    
                }
                if(activityMonitor.dailyCalorieDate.length > 3) {
                    tracker.dailyCalorieData = activityMonitor.dailyCalorieDate;//Sim added for calorie daily value- WEL-285
                }
                if(activityMonitor.dailyDistanceData.length > 3) {
                    tracker.dailyDistanceData = activityMonitor.dailyDistanceData; //Sim added for distance daily value- WEL-285
                }
                tracker.sleep = activityMonitor.sleep.intValue == 0 ? tracker.sleep : activityMonitor.sleep; //  WEL-302 activityMonitor.sleep;
                
                tracker.uuid = activityMonitor.uuid;
                tracker.startTimeInterval = activityMonitor.startTimeInterval;
                tracker.endTimeInterval = activityMonitor.endTimeInterval;
                tracker.modified_date = [NSDate date];
                tracker.sync_required = activityMonitor.sync_required;
                tracker.deviceType = activityMonitor.deviceType ;
                tracker.blockIndex = activityMonitor.blockIndex;
                tracker.bpm = activityMonitor.bpm.intValue > 40 ? activityMonitor.bpm : tracker.bpm;
                
                NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
                [df2 setDateFormat:@"yy/MM/dd"];
                
                NSDate *savedMeasurementDate = [df2 dateFromString:activityMonitor.dateString];
                NSString *savedMeasurementDateString = [df2 stringFromDate:savedMeasurementDate];
                NSString *currentSystemDate = [df2 stringFromDate:[NSDate date]];
                tracker.measurementRecieveDateTime = activityMonitor.measurementReceivedDateTime ? activityMonitor.measurementReceivedDateTime : currentDate;
                if ([savedMeasurementDateString isEqualToString:currentSystemDate]) {
                    tracker.measurementRecieveDateTime = currentDate;
                }
                
                NSError *errorUpdate = nil;
                if (activityMonitor.isRecordSavedFromThread) {
                    /*
                     if ([context hasChanges]) {
                     [context save:&errorUpdate];
                     [context reset];
                     }
                     */
                }
                else {
                    [context save:&errorUpdate];
                }
            }
            
        }
        else { //Insert Record...
            tracker = [NSEntityDescription insertNewObjectForEntityForName:@"ActivityTracker"
                                                    inManagedObjectContext:context];
            tracker.deviceType = activityMonitor.deviceType;
            tracker.dateString = activityMonitor.dateString;
            tracker.calories = activityMonitor.calories;
            tracker.distances = activityMonitor.distances; //activityMonitor.distances;
            tracker.steps = activityMonitor.steps;
            tracker.sleep = activityMonitor.sleep;
            tracker.uuid = activityMonitor.uuid;
            tracker.blockIndex = activityMonitor.blockIndex;
            tracker.startTimeInterval = activityMonitor.startTimeInterval;
            tracker.endTimeInterval = activityMonitor.endTimeInterval;
            tracker.created_date = currentDate;
            tracker.modified_date = currentDate;
            tracker.sync_required = activityMonitor.sync_required;
            tracker.bpm = activityMonitor.bpm;
            tracker.unit = activityMonitor.unit;
            tracker.dailyData = activityMonitor.dailyData; //Sim added for daily data design
            tracker.dailyCalorieData = activityMonitor.dailyCalorieDate; //Sim added for calorie daily value- WEL-285
            tracker.dailyDistanceData = activityMonitor.dailyDistanceData; //Sim added for distance daily value- WEL-285
            tracker.measurementRecieveDateTime = activityMonitor.measurementReceivedDateTime ? activityMonitor.measurementReceivedDateTime : currentDate;
            //UW-302 Sends these values for the sleep
            tracker.sleepStatus = activityMonitor.sleepStatus;
            tracker.sleepHours = activityMonitor.sleepHours;
            tracker.sleepMin = activityMonitor.sleepMin;

            [user addUserActivityObject:tracker];
            [tracker setUserActivity:user];
            
            //[context save:NULL];
            NSError *errorUpdate = nil;
            if (activityMonitor.isRecordSavedFromThread) {
                /*
                 if ([context hasChanges]) {
                 [context save:&errorUpdate];
                 [context reset];
                 }
                 */
            }
            else {
                [context save:&errorUpdate];
            }
            
        }
    }
}

+(void)moveActivitiesRecords:(ActivityTracker *)tracker
              forCurrentUser:(Users *)currentUser
             forSelectedUser:(Users *)selectedUser
                  forContext:(NSManagedObjectContext *)context
                       error:(NSError *__autoreleasing *)err
{
    @try
    {
        //Insert Record...
        //NSDate *date = [NSDate date];
        [selectedUser addUserActivityObject:tracker];
        [currentUser removeUserActivityObject:tracker];

        [tracker setUserActivity:selectedUser];
        tracker.sync_required = [NSNumber numberWithBool:YES];
        //tracker.dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:date];
        
        NSError *error = nil;
        
        if (![context save:&error]) {
            NSLog(@"%@",error.description);
        }
        
        
    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        *err = error;
    }
    @finally {
    }
}

@end
