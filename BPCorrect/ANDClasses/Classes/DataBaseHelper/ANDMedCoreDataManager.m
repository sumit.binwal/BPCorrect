//
//  ANDMedCoreDataManager.m
//  CoreDataDemo
//
//  Created by Vishal Lohia on 3/5/14.
//  Copyright (c) 2014 Vishal Lohia. All rights reserved.
//

#import "ANDMedCoreDataManager.h"
#import "Users.h"
#import "ActivityTracker.h"
#import "BloodPressureTracker.h"
#import "WeightScale.h"
#import "CommonClass.h"
#import "ANDMedGlobalConstants.h"

#ifndef appType //US app
#endif

@interface ANDMedCoreDataManager()
@property (nonatomic, copy) refreshActivitiesCompletion activityBlock;
@property (nonatomic, copy) refreshBloodPressureCompletion bloodPressureBlock;
@property (nonatomic, copy) refreshWeightScaleCompletion weightScaleBlock;
@property (nonatomic, copy) refreshThermometerCompletion thermometerBlock; //Sim added for Thermometer
@end

@implementation ANDMedCoreDataManager
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize fetchedControllerLiftTrackActivity = _fetchedControllerLiftTrackActivity;
@synthesize fetchedControllerBloodPressure = _fetchedControllerBloodPressure;
@synthesize fetchedControllerWeightScale = _fetchedControllerWeightScale;
@synthesize fetchedControllerThermometer = _fetchedControllerThermometer; //Sim added for Thermometer

@synthesize activityBlock = _activityBlock;
@synthesize currentBloodPressureInfo = _currentBloodPressureInfo;

@synthesize bloodPressureBlock = _bloodPressureBlock;
@synthesize currentWeightScaleInfo = _currentWeightScaleInfo;
@synthesize currentThermometerInfo = _currentThermometerInfo; //Sim added for Thermometer

+(ANDMedCoreDataManager *) sharedManager
{
    static dispatch_once_t onceToken;
    static ANDMedCoreDataManager *initObj = nil;
    
    dispatch_once(&onceToken, ^{
        initObj = [[self alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:initObj
                                                 selector:@selector(applicationWillTerminate:)
                                                     name:UIApplicationWillTerminateNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:initObj
                                                 selector:@selector(contextChanged:)
                                                     name:NSManagedObjectContextDidSaveNotification object:nil];
    });
    
    return initObj;
}


-(void) resetOnLogout

{
  //  [UserPreference setObject:nil forKey:@"isCM"];
  //  [UserPreference setObject:nil forKey:@"isCredExists"];
    
    self.currentActivity = nil;
    self.currentBloodPressureInfo = nil;
    self.currentWeightScaleInfo = nil;
    self.currentThermometerInfo = nil; //Sim added for Thermometer
    
    self.currentUser = nil;
    _fetchedControllerLiftTrackActivity = nil;
    _fetchedControllerBloodPressure = nil;
    _fetchedControllerWeightScale = nil;
    _fetchedControllerThermometer = nil; //Sim added for Thermometer
    
    
    @synchronized(self) {
        
        self.activityBlock = nil;
        self.bloodPressureBlock = nil;
        self.weightScaleBlock = nil;
        self.thermometerBlock = nil; //Sim added for Thermometer
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            abort();
        }
    }
}



#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ANDMedicalDataArchiver"
                                              withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ANDMedicalDataArchiver.sqlite"];
    //Sim added for automatic data migrations

   /* NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];*/
     NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {//Sim modified for migration
    
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark-
#pragma mark Common Function

-(NSInteger) totalRecordInEntityName:(NSString *)entityName
                        forPredicate:(NSPredicate *)p
                           inContext:(NSManagedObjectContext *)context
{
    
    @try {
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
        request.predicate = p;
        
        NSError *error = nil;
        NSInteger total = [context countForFetchRequest:request error:&error];
        
        if (error) {
            [NSException raise:error.domain format:@"%@",error.localizedDescription];
        }
        
        return total;
    }
    @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIAlertView showAlertWithException:exception shouldDisplayExceptionTitle:NO];
        });
    }
    @finally {
    }
    
    return 0;
}

-(NSArray *) maximumOrMinimumOrCountInContext:(NSManagedObjectContext *)context
                                   entityName:(NSString *)entity
                                attributeName:(NSString *)attrName
                                    predicate:(NSPredicate *)p
                            aggregateFunction:(NSString *)aggrFunction
{
    
    NSPredicate *pre_user = nil;
    if ([entity isEqualToString:@"ActivityTracker"]) {
        pre_user = [NSPredicate predicateWithFormat:@"userActivity.user_name = %@",self.currentUser.user_name];
    }
    else if ([entity isEqualToString:@"WeightScale"]) {
        pre_user = [NSPredicate predicateWithFormat:@"user.user_name = %@",self.currentUser.user_name];
        
    }
    else if ([entity isEqualToString:@"BloodPressureTracker"]) {
        pre_user = [NSPredicate predicateWithFormat:@"userBloodPressure.user_name = %@",self.currentUser.user_name];
        
    }
    else if ([entity isEqualToString:@"Thermometer"]) {
        pre_user = [NSPredicate predicateWithFormat:@"userThermometer.user_name = %@",self.currentUser.user_name]; //Sim added for Thermometer
        
    }
    p = p ? [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType
                                        subpredicates:@[p,pre_user]] : pre_user;
    
    // sample using attribute named "position" on entity "Item"
    NSFetchRequest *request = [NSFetchRequest new];
    request.entity = [NSEntityDescription entityForName:entity
                                 inManagedObjectContext:context];
    
    request.resultType = NSDictionaryResultType;
    // technically not necessary to set fetchLimit = 1, resultType could be (Item *) managed object
    
    if (p)
        request.predicate = p;
    
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath:attrName];
    NSExpression *maxExpression = [NSExpression expressionForFunction:aggrFunction arguments: @[ keyPathExpression ]];
    NSExpressionDescription *expression = [NSExpressionDescription new];
    
    expression.name = attrName;
    expression.expression = maxExpression;
    
    
    NSDictionary *attr_dict = [request.entity attributesByName];
    NSAttributeDescription *attr_disc = [attr_dict objectForKey:attrName];
    
    // This changes the return value in valueForKey: below
    expression.expressionResultType = attr_disc.attributeType;
    
    request.propertiesToFetch = @[ expression ];
    
    // Execute the fetch.
    NSError *error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    return objects;
}


#pragma mark-
#pragma mark User Defined Methods For Creating User and Fetching User

-(BOOL) initializeUserWithUserName:(NSString *)userName
                       andPassword:(NSString *)password
                             error:(NSError **)error
{
    @try {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *user = [NSEntityDescription entityForName:@"Users"
                                                inManagedObjectContext:self.managedObjectContext];
        
        NSPredicate *p = [NSPredicate predicateWithFormat:@"user_name = %@ AND user_password = %@ AND is_deleted = %@"
                          ,userName,password,[NSNumber numberWithBool:NO]];
        
        fetchRequest.entity = user;
        fetchRequest.predicate = p;
        
        NSError *err = nil;
        NSArray *arrUsers = [self.managedObjectContext executeFetchRequest:fetchRequest error:&err];
        
        if (error && err) {
            *error = err;
            return NO;
        }
        
        if (arrUsers.count == 0) {
            return NO;
        }
        
        self.currentUser = [arrUsers lastObject];
        
        return YES;
    }
    @catch (NSException *exception) {
        NSError *err = [CommonClass getErrorForException:exception];
        *error = err;
        return NO;
    }
    @finally {
    }
    
    return NO;
}

-(BOOL) insertAndInitializeGuestUser:(NSString *)userName
                         andPassword:(NSString *)password
                               error:(NSError **)error
{
    
    NSError *objError = nil;
    
    if ([self initializeUserWithUserName:userName
                             andPassword:password
                                   error:&objError] ==  NO) {
        
        if (objError) {
            *error = objError;
            return NO;
        }
        
        Users *objUser = [NSEntityDescription insertNewObjectForEntityForName:@"Users"
                                                       inManagedObjectContext:self.managedObjectContext];
        objUser.user_name = userName;
        objUser.user_password = password;
        objUser.created_date = [NSDate date];
        // objUser.user_firstName = @"Guest";
        //Sim commented for localization
        objUser.user_firstName = NSLocalizedString(@"TextBox.FirstName.Title", @""); //Sim modified for localization
        objUser.user_height = [NSNumber numberWithInt:70];
        objUser.user_weight = [NSNumber numberWithFloat:120];
        objUser.user_metric = @"feet";
        
        
        NSError *e = nil;
        [self.managedObjectContext save:&e];
        
        if (error && e) {
            *error = e;
            return NO;
        }
        
        self.currentUser = objUser;
        return YES;
    }
    
    return  YES;
}

#pragma mark-
#pragma mark Fetch Results Controller...

- (NSFetchedResultsController *)fetchedResultsControllerForEntity:(NSString *)entityName
{
    
    NSFetchedResultsController *_fetchedResultsController = nil;
    
    if (_fetchedResultsController == nil) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                                  inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"measurementRecieveDateTime"
                                                                       ascending:kIsActivityFetchOrderAscending];
        
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];//sortDescriptor
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                    managedObjectContext:self.managedObjectContext
                                                                                                      sectionNameKeyPath:nil
                                                                                                               cacheName:nil];
        
        _fetchedResultsController = aFetchedResultsController;
        _fetchedResultsController.delegate = (id)self;
        
        NSError *error = nil;
        
        if (![_fetchedResultsController performFetch:&error]) {
            abort();
        }
        
    }
	
	return _fetchedResultsController;
}

- (NSFetchedResultsController *)fetchedResultsControllerForEntity:(NSString *)entityName
                                                        predicate:(NSPredicate *)p
                                                         sortDisc:(NSArray *)arrSortDisc
{
    
    NSFetchedResultsController *_fetchedResultsController = nil;
    
    // Set up the fetched results controller if needed.
    if (_fetchedResultsController == nil) {
        // Create the fetch request for the entity.
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        // Edit the entity name as appropriate.
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                                  inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        [fetchRequest setPredicate:p];
        
        if (arrSortDisc.count == 0) {
            // Edit the sort key as appropriate.
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"measurementRecieveDateTime"
                                                                           ascending:kIsActivityFetchOrderAscending];
            /*
             NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"dateString"
             ascending:kIsActivityFetchOrderAscending];
             */
            
            NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];//sortDescriptor2
            
            [fetchRequest setSortDescriptors:sortDescriptors];
        }
        else {
            [fetchRequest setSortDescriptors:arrSortDisc];
        }
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                    managedObjectContext:self.managedObjectContext
                                                                                                      sectionNameKeyPath:nil
                                                                                                               cacheName:nil];
        
        _fetchedResultsController = aFetchedResultsController;
        _fetchedResultsController.delegate = (id)self;
        
        NSError *error = nil;
        
        if (![_fetchedResultsController performFetch:&error]) {
            abort();
        }
        
    }
	
	return _fetchedResultsController;
}

- (NSFetchedResultsController *) fetchedControllerLiftTrackActivity
{
    if (_fetchedControllerLiftTrackActivity == nil) {
        
        NSPredicate *predicate = [self getPredicateForCurrentUserForEntity:@"ActivityTracker"];
        
        _fetchedControllerLiftTrackActivity = [self fetchedResultsControllerForEntity:@"ActivityTracker"
                                                                            predicate:predicate
                                                                             sortDisc:nil];
    }
    
    return _fetchedControllerLiftTrackActivity;
}

- (NSFetchedResultsController *) fetchedControllerBloodPressure
{
    if (_fetchedControllerBloodPressure == nil) {
        
        NSPredicate *predicate = [self getPredicateForCurrentUserForEntity:@"BloodPressureTracker"];
        
        _fetchedControllerBloodPressure = [self fetchedResultsControllerForEntity:@"BloodPressureTracker"
                                                                        predicate:predicate
                                                                         sortDisc:nil];
        
        
        //_fetchedControllerBloodPressure = [self fetchedResultsControllerForEntity:@"BloodPressureTracker"];
    }
    
    
    return _fetchedControllerBloodPressure;
    
}

- (NSFetchedResultsController *) fetchedControllerWeightScale
{
    if (_fetchedControllerWeightScale == nil) {
        
        NSPredicate *predicate = [self getPredicateForCurrentUserForEntity:@"WeightScale"];
        
        _fetchedControllerWeightScale = [self fetchedResultsControllerForEntity:@"WeightScale"
                                                                      predicate:predicate
                                                                       sortDisc:nil];
        
        
        //  _fetchedControllerWeightScale = [self fetchedResultsControllerForEntity:@"WeightScale"];
    }
    
    return _fetchedControllerWeightScale;
}

/*Sim added for thermometer */
- (NSFetchedResultsController *) fetchedControllerThermometer
{
    if (_fetchedControllerThermometer == nil) {
        
        NSPredicate *predicate = [self getPredicateForCurrentUserForEntity:@"Thermometer"];
        
        _fetchedControllerThermometer = [self fetchedResultsControllerForEntity:@"Thermometer"
                                                                      predicate:predicate
                                                                       sortDisc:nil];
        
        
        //  _fetchedControllerWeightScale = [self fetchedResultsControllerForEntity:@"WeightScale"];
    }
    
    return _fetchedControllerThermometer;
}
#pragma mark-
#pragma mark Get All object information

-(NSArray *) getAllActivityDataForEntityName:(NSString *)entity
{
    @try {
        
        if ([entity isEqualToString:NSStringFromClass([ActivityTracker class])]) {
            NSError *error = nil;
            NSArray *arr = [ActivityTracker getSortedStartDateActiveActivitiesForContext:self.managedObjectContext
                                                                                 forUser:self.currentUser
                                                                                   error:&error];
            
            if (error) {
                [CommonClass showAlertForError:error];
                return nil;
            }
            
            return arr;
        }
        else if ([entity isEqualToString:NSStringFromClass([BloodPressureTracker class])])
        {
            NSError *error = nil;
            NSArray *arr = [BloodPressureTracker getSortedStartDateBloodPressureInfoForContext:self.managedObjectContext
                                                                                       forUser:self.currentUser
                                                                                     predicate:nil
                                                                                         error:&error];
            return arr;
            
        }
        else if ([entity isEqualToString:NSStringFromClass([WeightScale class])]) {
            return nil;
        }
//        else if ([entity isEqualToString:NSStringFromClass([Thermometer class])]) { //Sim added for Thermometer
//            return nil;
//        }
    }
    @catch (NSException *exception) {
        [CommonClass showAlertForError:[exception exceptionError]];
        return nil;
    }
    @finally {
    }
}

#pragma mark-
#pragma mark

-(NSPredicate *) getPredicateForCurrentUserForEntity:(NSString *)entity
{
    NSPredicate *predicate = nil;
    
    if ([entity isEqualToString:@"ActivityTracker"]) {
        predicate = [NSPredicate predicateWithFormat:@"userActivity.user_name = %@",self.currentUser.user_name];
    }
    else if ([entity isEqualToString:@"BloodPressureTracker"])
    {
        predicate = [NSPredicate predicateWithFormat:@"userBloodPressure.user_name = %@",self.currentUser.user_name];
    }
    else if ([entity isEqualToString:@"WeightScale"])
    {
        predicate = [NSPredicate predicateWithFormat:@"user.user_name = %@",self.currentUser.user_name];
    }
    else if ([entity isEqualToString:@"Thermometer"]) //Sim added for Thermometer
    {
        predicate = [NSPredicate predicateWithFormat:@"userThermometer.user_name = %@",self.currentUser.user_name];
    }
    return predicate;
}

#pragma mark-
#pragma mark Generic Method To Get Max or Min of dateString for Entity

-(NSString *) getMaxMinDateStringFromDBForEntity:(NSString *)entity
                                 forActivityType:(ActivityDateType)activitydateType
{
    if (activitydateType == ActivityDateType_None) {
        return nil;
    }
    
    NSString *aggrFunction = ((activitydateType == ActivityDateType_Max)
                              ? @"max:" : @"min:");
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:entity
                                                   attributeName:@"dateString"
                                                       predicate:nil
                                               aggregateFunction:aggrFunction];
    
    return [[maxDateObj lastObject] objectForKey:@"dateString"];
    
}


#pragma mark-
#pragma mark Activity Tracker Fetcher Methods...

-(void) refreshDataManagerWithUserActivities:(refreshActivitiesCompletion)blk
{
    @try {
        
        if (self.arr_userActivities == nil) {
            self.arr_userActivities = [NSMutableArray array];
        }
        
        /*
         Following line of code is to get the maximum dateString value and fetch that activity to display on dashboard.
         */
        self.activityBlock = blk;
        NSError *error = nil;
        
        NSPredicate *pred_user = [NSPredicate predicateWithFormat:@"userActivity.user_name = %@",self.currentUser.user_name];
        NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                          entityName:@"ActivityTracker"
                                                       attributeName:@"measurementRecieveDateTime"
                                                           predicate:pred_user
                                                   aggregateFunction:@"max:"];
        
        NSPredicate *p = nil;
        if (maxDateObj.count)
            p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",[[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"]];
        
        NSArray *arr_Tracker = [ActivityTracker getSortedStartDateActiveActivitiesForContext:self.managedObjectContext
                                                                                     forUser:self.currentUser
                                                                                   predicate:p
                                                                                       error:&error];
        
        //Array will hold one object i.e maximum object
        @synchronized(self) {
            self.currentActivity = nil;
            if ([arr_Tracker count]) {
                [self.arr_userActivities removeAllObjects];
                [self.arr_userActivities addObjectsFromArray:arr_Tracker];
                self.currentActivity = [self.arr_userActivities objectAtIndex:0];
            }
        }
        
        if (self.activityBlock) {
            self.activityBlock(error);
            self.activityBlock = nil;
        }
        
    }
    @catch (NSException *exception) {
        NSError *error = [CommonClass getErrorForException:exception];
        [CommonClass showAlertForError:error];
    }
    @finally {
    }
}

-(NSDate *) getActivityMaxMinDateStringFromDB:(ActivityDateType)activitydateType
{
    if (activitydateType == ActivityDateType_None) {
        return nil;
    }
    
    NSString *aggrFunction = ((activitydateType == ActivityDateType_Max)
                              ? @"max:" : @"min:");
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"ActivityTracker"
                                                   attributeName:@"measurementRecieveDateTime"
                                                       predicate:nil
                                               aggregateFunction:aggrFunction];
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
    
}

-(NSDate *) getNextOrPreviousActivityDate:(BOOL)isNextActivity
                             toDateString:(NSDate *)dateString
{
    NSPredicate *p =  (isNextActivity ? [NSPredicate predicateWithFormat:@"measurementRecieveDateTime > %@",dateString]
                       : [NSPredicate predicateWithFormat:@"measurementRecieveDateTime < %@",dateString]);
    
    NSString *aggrFunction = isNextActivity ? @"min:" : @"max:";
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"ActivityTracker"
                                                   attributeName:@"measurementRecieveDateTime"
                                                       predicate:p
                                               aggregateFunction:aggrFunction];
    
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
}

-(NSDate *) hasActivity:(BOOL)isNext
{
    NSString *aggrFunction = isNext ? @"max:" : @"min:";
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"ActivityTracker"
                                                  attributeName:@"measurementRecieveDateTime"
                                                    predicate:nil
                                               aggregateFunction:aggrFunction];
    
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
}

-(NSString *) hasActivityNew:(BOOL)isNext
{
    NSString *aggrFunction = isNext ? @"max:" : @"min:";
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"ActivityTracker"
                                                   attributeName:@"dateString"
                                                       predicate:nil
                                               aggregateFunction:aggrFunction];
    
   
    return [[maxDateObj lastObject] objectForKey:@"dateString"];
}


-(BOOL) hasPreviousActivity
{
    ActivityTracker *current = self.currentActivity;
    
    if (current) {
       if ([current.dailyData isEqualToString:@"NA"]) {
            //Sim  add logic for 10 min new design
           NSString *maxDateStringValue = [self hasActivityNew:NO];
           if ([current.dateString isEqualToString:maxDateStringValue]) {
               //Same dateString hence there is no next value
               return NO;
           }
           
           return YES;
           
        } else {
            //Old basic design - retain older code 
            NSDate *minDateStringValue = [self hasActivity:NO];
            
            if ([current.measurementRecieveDateTime compare:minDateStringValue] == NSOrderedSame)
            {
                return NO;
            }
            
            return YES;
        }
        
       
    }
    
    return NO;
}

-(BOOL) hasNextActivity
{
    ActivityTracker *current = self.currentActivity;
    if (current)
    {
        
        //Sim - new logic for 10 min data
        if ([current.dailyData isEqualToString:@"NA"]) {
            NSString *maxDateStringValue = [self hasActivityNew:YES];
           if ([current.dateString isEqualToString:maxDateStringValue]) {
               //Same dateString hence there is no next value
                return NO;
            }
            
            return YES;
        } else {
        //If the value does not contain the 10 min data then use the old logic
            NSDate *maxDateStringValue = [self hasActivity:YES];
            if ([current.measurementRecieveDateTime compare:maxDateStringValue] == NSOrderedSame) {
                return NO;
            }
            
            return YES;
        }
       
    }
    
    return NO;

    }

-(BOOL) nextActivity
{
    @try {
        @synchronized(self) {
            
            if ([self hasNextActivity])
            {
                //Sim add a new design if it has 10 minutes implemented
                //Sim adding the logic for the 10 min new design
                if ([self.currentActivity.dailyData isEqualToString:@"NA"]) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateString = %@ ",self.currentActivity.dateString];
                    NSFetchRequest *request = [[NSFetchRequest alloc] init];
                    request.entity = [NSEntityDescription entityForName:@"ActivityTracker" inManagedObjectContext:[ANDMedCoreDataManager sharedManager].managedObjectContext];
                    request.predicate = predicate;
                    NSError *error1 = nil;
                    NSArray *array = [[ANDMedCoreDataManager sharedManager].managedObjectContext executeFetchRequest:request error:&error1];
                    
                    //Sorting the array based on the measurement Received time
                    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"measurementRecieveDateTime"
                                                                                 ascending:YES];
                    NSArray *results = [array
                                        sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
                    
                    self.currentActivity = [results lastObject];
                  
                } //End of 10 min new design

                
                NSDate *nextDateString = [self getNextOrPreviousActivityDate:YES
                                                                toDateString:self.currentActivity.measurementRecieveDateTime];
                NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",nextDateString];
                NSError *error = nil;
                NSArray *arr = [ActivityTracker getSortedStartDateActiveActivitiesForContext:self.managedObjectContext
                                                                                     forUser:self.currentUser
                                                                                   predicate:p
                                                                                       error:&error];
                self.currentActivity = [arr lastObject];
                
                return self.currentActivity ? YES : NO;
            }
            else {
                return NO;
            }
        }
    }
    @catch (NSException *exception) {
        [CommonClass showAlertForException:exception];
        return NO;
    }
    @finally {
    }
}

-(BOOL) previousActivity
{
    @try {
        if (self.hasPreviousActivity) {
            //Sim adding the logic for the 10 min new design
            if ([self.currentActivity.dailyData isEqualToString:@"NA"]) {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateString = %@ ",self.currentActivity.dateString];
                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                request.entity = [NSEntityDescription entityForName:@"ActivityTracker" inManagedObjectContext:[ANDMedCoreDataManager sharedManager].managedObjectContext];
                request.predicate = predicate;
                NSError *error1 = nil;
                NSArray *array = [[ANDMedCoreDataManager sharedManager].managedObjectContext executeFetchRequest:request error:&error1];
               
                //Sorting the array based on the measurement Received time
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"measurementRecieveDateTime"
                                                                             ascending:YES];
                NSArray *results = [array
                                    sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
               
                self.currentActivity = [results firstObject];
            } //End of 10 min new design
    
            NSDate *prevDateString = [self getNextOrPreviousActivityDate:NO
                                                            toDateString:self.currentActivity.measurementRecieveDateTime];
            NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",prevDateString];
            NSError *error = nil;
            NSArray *arr = [ActivityTracker getSortedStartDateActiveActivitiesForContext:self.managedObjectContext
                                                                                 forUser:self.currentUser
                                                                               predicate:p
                                                                                   error:&error];
            self.currentActivity = [arr lastObject];
            
            return self.currentActivity ? YES : NO;
        }
        else {
            return NO;
        }
    }
    @catch (NSException *exception) {
        [CommonClass showAlertForException:exception];
    }
    @finally {
    }
}

#pragma mark-
#pragma mark Blood Pressure Methods....

-(NSDate *) getBloodPressureMaxMinDateStringFromDB:(ActivityDateType)activitydateType
{
    if (activitydateType == ActivityDateType_None) {
        return nil;
    }
    
    NSString *aggrFunction = ((activitydateType == ActivityDateType_Max)
                              ? @"max:" : @"min:");
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"BloodPressureTracker"
                                                   attributeName:@"measurementRecieveDateTime"
                                                       predicate:nil
                                               aggregateFunction:aggrFunction];
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
    
}

-(NSDate *) getNextOrPreviousBloodPressureInfoDate:(BOOL)isNextActivity
                                      toDateString:(NSDate *)dateString
{
    NSPredicate *p =  (isNextActivity ? [NSPredicate predicateWithFormat:@"measurementRecieveDateTime > %@",dateString]
                       : [NSPredicate predicateWithFormat:@"measurementRecieveDateTime < %@",dateString]);
    
    NSString *aggrFunction = isNextActivity ? @"min:" : @"max:";
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"BloodPressureTracker"
                                                   attributeName:@"measurementRecieveDateTime"
                                                       predicate:p
                                               aggregateFunction:aggrFunction];
    
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
}


-(void) refreshDataManagerWithUserBloodPressureInfo:(refreshBloodPressureCompletion)blk
{
    @try {
        
        /*
         Following line of code is to get the maximum dateString value and fetch that activity to display on dashboard.
         */
        self.bloodPressureBlock = blk;
        NSError *error = nil;
        NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                          entityName:@"BloodPressureTracker"
                                                       attributeName:@"measurementRecieveDateTime"
                                                           predicate:nil
                                                   aggregateFunction:@"max:"];
        
        NSPredicate *p = nil;
        if (maxDateObj.count)
            p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",[[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"]];
        
        NSArray *arr_Tracker = [BloodPressureTracker getSortedStartDateBloodPressureInfoForContext:self.managedObjectContext
                                                                                           forUser:self.currentUser
                                                                                         predicate:p
                                                                                             error:&error];
        
        //Array will hold one object i.e maximum object
        @synchronized(self) {
            self.currentBloodPressureInfo = nil;
            if ([arr_Tracker count]) {
                self.currentBloodPressureInfo = [arr_Tracker objectAtIndex:0];
            }
        }
        
        if (self.bloodPressureBlock) {
            self.bloodPressureBlock(error);
            self.bloodPressureBlock = nil;
        }
        
    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        [CommonClass showAlertForError:error];
    }
    @finally {
    }
    
}

-(BOOL) hasPreviousBloodPressureInfoAvailable
{
    BloodPressureTracker *current = self.currentBloodPressureInfo;
    if (current)
    {
        NSDate *minDateStringValue = [self getBloodPressureMaxMinDateStringFromDB:ActivityDateType_Min];
        if ([current.measurementRecieveDateTime compare:minDateStringValue] == NSOrderedSame) {
            return NO;
        }
        
        return YES;
    }
    
    return NO;
}


-(BOOL) hasNextBloodPressureInfoAvailable
{
    BloodPressureTracker *current = self.currentBloodPressureInfo;
    
    if (current)
    {
        NSDate *maxDateValue = [self getBloodPressureMaxMinDateStringFromDB:ActivityDateType_Max];
        if ([current.measurementRecieveDateTime compare:maxDateValue] == NSOrderedSame) {
            return NO;
        }
        
        return YES;
    }
    
    return NO;
}

-(BOOL) nextBloodPressureInfo
{
    @try {
        if ([self hasNextBloodPressureInfoAvailable]) {
            
            NSDate *nextDateString = [self getNextOrPreviousBloodPressureInfoDate:YES
                                                                     toDateString:self.currentBloodPressureInfo.measurementRecieveDateTime];
            
            NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",nextDateString];
            NSError *error = nil;
            NSArray *arr = [BloodPressureTracker getSortedStartDateBloodPressureInfoForContext:self.managedObjectContext
                                                                                       forUser:self.currentUser
                                                                                     predicate:p
                                                                                         error:&error];
            self.currentBloodPressureInfo = [arr lastObject];
            
            return self.currentBloodPressureInfo ? YES : NO;
        }
        else {
            return NO;
        }
    }
    @catch (NSException *exception) {
        [CommonClass showAlertForException:exception];
    }
    @finally {
    }
    
    return NO;
}



-(BOOL) previousBloodPressureInfo
{
    @try {
        if ([self hasPreviousBloodPressureInfoAvailable]) {
            
            NSDate *prevDateString = [self getNextOrPreviousBloodPressureInfoDate:NO
                                                                     toDateString:self.currentBloodPressureInfo.measurementRecieveDateTime];
            
            NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",prevDateString];
            NSError *error = nil;
            NSArray *arr = [BloodPressureTracker getSortedStartDateBloodPressureInfoForContext:self.managedObjectContext
                                                                                       forUser:self.currentUser
                                                                                     predicate:p
                                                                                         error:&error];
            self.currentBloodPressureInfo = [arr lastObject];
            
            return self.currentBloodPressureInfo ? YES : NO;
        }
        else {
            return NO;
        }
    }
    @catch (NSException *exception) {
        [CommonClass showAlertForException:exception];
    }
    @finally {
    }
    
    return NO;
}



#pragma mark-
#pragma mark Core Data Context Changed Method....

- (void)contextChanged:(NSNotification*)notification
{
    if ([notification object] == self.managedObjectContext) return;
    
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(contextChanged:) withObject:notification waitUntilDone:YES];
        return;
    }
    [[self managedObjectContext] mergeChangesFromContextDidSaveNotification:notification];
}


#pragma mark - Weight SCale Methods...


-(void) refreshDataManagerWithUserWeightScaleInfo:(refreshWeightScaleCompletion)blk
{
    @try {
        
        /*
         Following line of code is to get the maximum dateString value and fetch that activity to display on dashboard.
         */
        self.weightScaleBlock = blk;
        NSError *error = nil;
        NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                          entityName:@"WeightScale"
                                                       attributeName:@"measurementRecieveDateTime"
                                                           predicate:nil
                                                   aggregateFunction:@"max:"];
        
        NSPredicate *p = nil;
        if (maxDateObj.count)
            p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",[[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"]];
        
        
        NSArray *arr_tracker = [WeightScale getSortedStartDateWeightScaleInfoForContext:self.managedObjectContext
                                                                                forUser:self.currentUser
                                                                              predicate:p
                                                                                  error:&error];
        
        
        //Array will hold one object i.e maximum object
        @synchronized(self) {
            self.currentWeightScaleInfo = nil;
            if ([arr_tracker count]) {
                self.currentWeightScaleInfo = [arr_tracker lastObject];
            }
        }
        
        if (self.weightScaleBlock) {
            self.weightScaleBlock(error);
            self.weightScaleBlock = nil;
        }
        
    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        [CommonClass showAlertForError:error];
    }
    @finally {
    }
    
}

-(NSDate *) getWeightScaleMaxMinDateStringFromDB:(ActivityDateType)activitydateType
{
    if (activitydateType == ActivityDateType_None) {
        return nil;
    }
    
    NSString *aggrFunction = ((activitydateType == ActivityDateType_Max)
                              ? @"max:" : @"min:");
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"WeightScale"
                                                   attributeName:@"measurementRecieveDateTime"
                                                       predicate:nil
                                               aggregateFunction:aggrFunction];
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
    
}

-(NSDate *) getNextOrPreviousWeightScaleInfoDate:(BOOL)isNextActivity
									toDateString:(NSDate *)dateString
{
    NSPredicate *p =  (isNextActivity ? [NSPredicate predicateWithFormat:@"measurementRecieveDateTime > %@",dateString]
                       : [NSPredicate predicateWithFormat:@"measurementRecieveDateTime < %@",dateString]);
    
    NSString *aggrFunction = isNextActivity ? @"min:" : @"max:";
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"WeightScale"
                                                   attributeName:@"measurementRecieveDateTime"
                                                       predicate:p
                                               aggregateFunction:aggrFunction];
    
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
}


-(BOOL) hasPreviousWeightScaleInfoAvailable
{
    WeightScale *current = self.currentWeightScaleInfo;
    
    if (current)
    {
        NSDate *minDateStringValue = [self getWeightScaleMaxMinDateStringFromDB:ActivityDateType_Min];
        if ([current.measurementRecieveDateTime compare:minDateStringValue] == NSOrderedSame) {
            return NO;
        }
        
        return YES;
    }
    
    return NO;
}

-(BOOL) hasNextWeightScaleInfoAvailable
{
    WeightScale *current = self.currentWeightScaleInfo;
    
    if (current)
    {
        NSDate *maxDateValue = [self getWeightScaleMaxMinDateStringFromDB:ActivityDateType_Max];
        if ([current.measurementRecieveDateTime compare:maxDateValue] == NSOrderedSame) {
            return NO;
        }
        
        return YES;
    }
    
    return NO;
}

-(BOOL) nextWeightScaleInfo
{
    @try {
        if ([self hasNextWeightScaleInfoAvailable]) {
            
            NSDate *nextDateString = [self  getNextOrPreviousWeightScaleInfoDate:YES
																	toDateString:self.currentWeightScaleInfo.measurementRecieveDateTime];
            
            NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",nextDateString];
            NSError *error = nil;
            NSArray *arr = [WeightScale getSortedStartDateWeightScaleInfoForContext: self.managedObjectContext
																			forUser:self.currentUser
																		  predicate:p
																			  error:&error];
			
            self.currentWeightScaleInfo = [arr lastObject];
            
            return self.currentWeightScaleInfo ? YES : NO;
        }
        else {
            return NO;
        }
    }
    @catch (NSException *exception) {
        [CommonClass showAlertForException:exception];
    }
    @finally {
    }
    
    return NO;
}

-(BOOL) previousWeightScaleInfo
{
    @try {
        if ([self hasPreviousWeightScaleInfoAvailable]) {
            
            NSDate *prevDateString = [self  getNextOrPreviousWeightScaleInfoDate:NO
                                                                    toDateString:self.currentWeightScaleInfo.measurementRecieveDateTime];
            
            NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",prevDateString];
            NSError *error = nil;
            NSArray *arr = [WeightScale getSortedStartDateWeightScaleInfoForContext: self.managedObjectContext
                                                                            forUser:self.currentUser
                                                                          predicate:p
                                                                              error:&error];
            self.currentWeightScaleInfo = [arr lastObject];
            
            return self.currentBloodPressureInfo ? YES : NO;
        }
        else {
            return NO;
        }
    }
    @catch (NSException *exception) {
        [CommonClass showAlertForException:exception];
    }
    @finally {
    }
    
    return NO;
}

/*Sim changes for Thermometer */
#pragma mark - Thermometer Methods...


-(void) refreshDataManagerWithUserThermometerInfo:(refreshThermometerCompletion)blk
{
//    @try {
//        
//        /*
//         Following line of code is to get the maximum dateString value and fetch that activity to display on dashboard.
//         */
//        self.thermometerBlock = blk;
//        NSError *error = nil;
//        NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
//                                                          entityName:@"Thermometer"
//                                                       attributeName:@"measurementRecieveDateTime"
//                                                           predicate:nil
//                                                   aggregateFunction:@"max:"];
//        
//        NSPredicate *p = nil;
//        if (maxDateObj.count)
//            p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",[[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"]];
//        
//        
//        NSArray *arr_tracker = [Thermometer getSortedStartDateTemperatureInfoForContext:self.managedObjectContext
//                                                                                forUser:self.currentUser
//                                                                              predicate:p
//                                                                                  error:&error];
//        
//        
//        //Array will hold one object i.e maximum object
//        @synchronized(self) {
//            self.currentThermometerInfo = nil;
//            if ([arr_tracker count]) {
//                self.currentThermometerInfo = [arr_tracker lastObject];
//            }
//        }
//        
//        if (self.thermometerBlock) {
//            self.thermometerBlock(error);
//            self.thermometerBlock = nil;
//        }
//        
//    }
//    @catch (NSException *exception) {
//        NSError *error = [exception exceptionError];
//        [CommonClass showAlertForError:error];
//    }
//    @finally {
//    }
    
}

-(NSDate *) getThermometerMaxMinDateStringFromDB:(ActivityDateType)activitydateType
{
    if (activitydateType == ActivityDateType_None) {
        return nil;
    }
    
    NSString *aggrFunction = ((activitydateType == ActivityDateType_Max)
                              ? @"max:" : @"min:");
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"Thermometer"
                                                   attributeName:@"measurementRecieveDateTime"
                                                       predicate:nil
                                               aggregateFunction:aggrFunction];
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
    
}

-(NSDate *) getNextOrPreviousThermometerInfoDate:(BOOL)isNextActivity
                                    toDateString:(NSDate *)dateString
{
    NSPredicate *p =  (isNextActivity ? [NSPredicate predicateWithFormat:@"measurementRecieveDateTime > %@",dateString]
                       : [NSPredicate predicateWithFormat:@"measurementRecieveDateTime < %@",dateString]);
    
    NSString *aggrFunction = isNextActivity ? @"min:" : @"max:";
    
    NSArray *maxDateObj = [self maximumOrMinimumOrCountInContext:self.managedObjectContext
                                                      entityName:@"Thermometer"
                                                   attributeName:@"measurementRecieveDateTime"
                                                       predicate:p
                                               aggregateFunction:aggrFunction];
    
    
    return [[maxDateObj lastObject] objectForKey:@"measurementRecieveDateTime"];
}


-(BOOL) hasPreviousThermometerInfoAvailable
{
//    Thermometer *current = self.currentThermometerInfo;
//
//    if (current)
//    {
//        NSDate *minDateStringValue = [self getThermometerMaxMinDateStringFromDB:ActivityDateType_Min];
//        if ([current.measurementRecieveDateTime compare:minDateStringValue] == NSOrderedSame) {
//            return NO;
//        }
//
//        return YES;
//    }
  
    return NO;
}

-(BOOL) hasNextThermometerInfoAvailable
{
    Thermometer *current = self.currentThermometerInfo;
    
//    if (current)
//    {
//        NSDate *maxDateValue = [self getThermometerMaxMinDateStringFromDB:ActivityDateType_Max];
//        if ([current.measurementRecieveDateTime compare:maxDateValue] == NSOrderedSame) {
//            return NO;
//        }
//
//        return YES;
//    }
  
    return NO;
}

-(BOOL) nextThermometerInfo {
//{
//    @try {
   //     if ([self hasNextThermometerInfoAvailable]) {
            
//            NSDate *nextDateString = [self  getNextOrPreviousThermometerInfoDate:YES
//                                                                    toDateString:self.currentThermometerInfo.measurementRecieveDateTime];
//
//            NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",nextDateString];
//            NSError *error = nil;
//            NSArray *arr = [Thermometer getSortedStartDateTemperatureInfoForContext: self.managedObjectContext
//                                                                            forUser:self.currentUser
//                                                                          predicate:p
//                                                                              error:&error];
//
//            self.currentThermometerInfo = [arr lastObject];
//
//            return self.currentThermometerInfo ? YES : NO;
//        }
//        else {
         //   return NO;
       // }
//    }
//    @catch (NSException *exception) {
//        [CommonClass showAlertForException:exception];
//    }
//    @finally {
//    }
    
    return NO;
}

-(BOOL) previousThermometerInfo
{
//    @try {
//        if ([self hasPreviousThermometerInfoAvailable]) {
//            
//            NSDate *prevDateString = [self  getNextOrPreviousThermometerInfoDate:NO
//                                                                    toDateString:self.currentThermometerInfo.measurementRecieveDateTime];
//            
//            NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",prevDateString];
//            NSError *error = nil;
//            NSArray *arr = [Thermometer getSortedStartDateTemperatureInfoForContext: self.managedObjectContext
//                                                                            forUser:self.currentUser
//                                                                          predicate:p
//                                                                              error:&error];
//            self.currentThermometerInfo = [arr lastObject];
//            
//            return self.currentThermometerInfo ? YES : NO;
//        }
//        else {
//            return NO;
//        }
//    }
//    @catch (NSException *exception) {
//        [CommonClass showAlertForException:exception];
//    }
//    @finally {
//    }
    
    return NO;
}
@end
