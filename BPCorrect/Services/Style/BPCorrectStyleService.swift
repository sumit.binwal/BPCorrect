//
//  BPCorrectStyleService.swift
//  BPCorrect
//
//  Created by "" on 21/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit

struct BPCorrectStyleService: StyleService {
    static func lightFontOfSize(_ size: CGFloat) -> UIFont {
        return UIFont.lightFontOfSize(size)!
    }
    
    static func boldFontOfSize(_ size: CGFloat) -> UIFont {
        return UIFont.boldFontOfSize(size)!
    }
    
    static func regularFontOfSize(_ size: CGFloat) -> UIFont {
        return UIFont.regularFontOfSize(size)!
    }
    
    

    static func bpCorrectThemeColor() -> UIColor {
        return UIColor.bpCorrectThemeColor()!
    }
    
    static func bpCorrectCGThemeColor() -> CGColor {
        return CGColor.bpCorrectCGThemeColor()!
    }
}
