//
//  RegisterVC.swift
//  BPCorrect
//
//  Created by "" on 31/03/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import Alamofire

class RegisterVC: UIViewController {
  
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var usernameTextfield: UITextField!
  @IBOutlet var passwordTextfield: UITextField!
  @IBOutlet var confirmPasswordTextfield: UITextField!
  @IBOutlet var registerButton: UIButton!
  
  var activeField: UITextField?
  var loadingView: LoadingView?
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.usernameTextfield.delegate = self
    self.passwordTextfield.delegate = self
    self.confirmPasswordTextfield.delegate = self
    
    registerButton.layer.cornerRadius = 3.0
    
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                             action: #selector(LoginVC.dismissKeyboard))
    view.addGestureRecognizer(tap)
    
    self.view.insetsLayoutMarginsFromSafeArea = true
    self.scrollView.insetsLayoutMarginsFromSafeArea = true
    
    registerForKeyboardNotifications()
    
    // Do any additional setup after loading the view.
  }
  
  
  @IBAction func loginButtonIsPressed(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    deregisterFromKeyboardNotifications()
  }
  
  func registerForKeyboardNotifications()
  {
    //Adding notifies on keyboard appearing
    NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  
  func deregisterFromKeyboardNotifications()
  {
    //Removing notifies on keyboard appearing
    NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  @objc func keyboardWillShow(notification: NSNotification) {
    
    //Need to calculate keyboard exact size due to Apple suggestions
    if(!self.scrollView.isScrollEnabled ) {
      self.scrollView.isScrollEnabled = true
    }
    
    let info : NSDictionary = notification.userInfo! as NSDictionary
    let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
    
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
    var aRect : CGRect = self.view.frame
    aRect.size.height -= keyboardSize!.height
    if activeField != nil
    {
      if (!aRect.contains(activeField!.frame.origin))
      {
        self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
      }
    }
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    //Once keyboard disappears, restore original positions
    let info : NSDictionary = notification.userInfo! as NSDictionary
    let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    // self.view.endEditing(true)
    // self.scrollView.isScrollEnabled = false
  }
  
  @objc func dismissKeyboard() {
    self.view.endEditing(true)
  }
  
  @IBAction func backButtonIsPressed(_ sender: Any) {
    self.view.endEditing(true)
    self.navigationController?.popViewController(animated: true)
  }
  
  func validateLoginForm() -> Bool {
    if usernameTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0 {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter username.")
      return false
    } else if passwordTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0  {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter password.")
      return false
    } else if confirmPasswordTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0  {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please confirm password.")
      return false
    } else if passwordTextfield.text != confirmPasswordTextfield.text  {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Passwords does not match")
      return false
    }
    return true
  }
  
  @IBAction func registerButtonIsPressed(_ sender: Any) {
    
    if validateLoginForm() {
      let params : Parameters = ["email":usernameTextfield.text!,
                                 "password":passwordTextfield.text!]
      
      setIsLoading(true)
      APIManager.sharedInstance.registerUser(params: params, completionHandler: { [weak self](registerModel) in
         guard let strongSelf = self else { return }
        strongSelf.setIsLoading(false)
        
        let dict : Dictionary<String,AnyObject> = registerModel.data![0]
        
        let userId : Int = dict["userId"] as? Int ?? 0
        let message : String = dict["message"] as? String ?? ""
        
        if userId != 0 {
          let alert = UIAlertController(title: "An account verfication link is sent to your email.", message: message, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self?.navigationController?.popViewController(animated: true)
          }))
          self!.present(alert, animated: true, completion: nil)
          
        } else if message == "user Already Exists" {
          AlertUtils.showAlertMessage(self!, withTitle: "", andMessage:"User Already Exists")
        } else {
          AlertUtils.showAlertMessage(self!, withTitle: "", andMessage:"Something went wrong. Please try again.")
        }
        
      }) { (error) in
        self.setIsLoading(false)
        AlertUtils.showAlertMessage(self, withTitle: "", andMessage:error.userInfo["NSLocalizedDescription"] as? String)
      }
    }
  }
}

extension RegisterVC : UITextFieldDelegate {
  /**
   * Called when 'return' key pressed. return NO to ignore.
   */
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == usernameTextfield{
      textField.resignFirstResponder()
      passwordTextfield.becomeFirstResponder()
    } else if textField == passwordTextfield {
      confirmPasswordTextfield.becomeFirstResponder()
    }  else if textField == passwordTextfield {
      textField.resignFirstResponder()
    }
    return true
  }
  
  func textField(_ textField: UITextField,
                 shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {
    
    if textField == self.passwordTextfield  {
      let nsString:NSString? = textField.text as NSString?
      let updatedString = nsString?.replacingCharacters(in:range, with:string)
      
      textField.text = updatedString
      
      
      //Setting the cursor at the right place
      let selectedRange = NSMakeRange(range.location + string.count, 0)
      let from = textField.position(from: textField.beginningOfDocument, offset:selectedRange.location)
      let to = textField.position(from: from!, offset:selectedRange.length)
      textField.selectedTextRange = textField.textRange(from: from!, to: to!)
      
      //Sending an action
      textField.sendActions(for: UIControl.Event.editingChanged)
      
      return false
    }
    
    return true
    
  }
}



extension RegisterVC: LoadingViewProtocol {
  
  func loadingMessage() -> String? {
    return nil
  }
}
