//
//  ChartDataResponseModel.swift
//  BPCorrect
//
//  Created by "" on 28/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import ObjectMapper

class ChartDataResponseModel : Mappable {
  
  var valid: Bool?
  var message: String?
  var data: [ChartData]?
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    valid <- map["valid"]
    message <- map["message"]
    data <- map["data"]
  }
}

class ChartData : Mappable {

  var chartdata: [ChartCompleteData]?
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    chartdata <- map["chartdata"]
  }
}



//{"message":"Get Chart Details.","data":[{"chartdata":[{"date":"4/28","avg_values":{"SBP":"105","DBP":"76"},"actual_values":[{"timestamp":"1556434621","protocol_id":"","is_abberant":"0","SBP":"107","DBP":"84"},{"timestamp":"1556439023","protocol_id":"","is_abberant":"0","SBP":"104","DBP":"73"},{"timestamp":"1556439131","protocol_id":"","is_abberant":"0","SBP":"103","DBP":"72"}]}]}],"valid":true}

class Avg_Values: Mappable {
  
  var dbp: String?
  var sbp: String?
  var pulse: String?
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    dbp <- map["DBP"]
    sbp <- map["SBP"]
    pulse <- map["PULSE"]
  }
}

class Actual_Values: Mappable {
  
  var dbp: String?
  var sbp: String?
  var pulse: String?
  var timestamp: String?
  var protocol_id: String?
  var is_abberant: String?

  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    dbp <- map["DBP"]
    sbp <- map["SBP"]
    pulse <- map["PULSE"]
    timestamp <- map["timestamp"]
    protocol_id <- map["protocol_id"]
    is_abberant <- map["is_abberant"]
  }
}

class ChartCompleteData: Mappable {
  
  var avg_values: Avg_Values?
  var actual_values: [Actual_Values]?
  var date: String?
  
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    avg_values <- map["avg_values"]
    actual_values <- map["actual_values"]
    date <- map["date"]
  }
}
